import { useEffect, useState } from 'react';
import './App.css';
import MainPage from './MainPage/MainPage';
import { api } from './utils/api';
import { ValueObjectOutput } from './utils/types';
import DateAdapter from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';

function App() {
  const [storages, setStorages] = useState<ValueObjectOutput[]>([]);
  const [products, setProducts] = useState<ValueObjectOutput[]>([]);

  useEffect(() => {
    api.getProducts({ limit: 100, offset: 0 }).then(it => setProducts(it.values));
    api.getStorages({ limit: 100, offset: 0 }).then(it => setStorages(it.values));
  }, []);

  return (
    <LocalizationProvider dateAdapter={DateAdapter}>
      <div className="App">
        <MainPage storages={storages} products={products} />
      </div>
    </LocalizationProvider>
  );
}

export default App;

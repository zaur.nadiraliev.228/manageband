import {
    Alert,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormLabel,
    Grid,
    Input,
    InputLabel,
    MenuItem,
    Select,
    Snackbar
} from "@mui/material";
import {useCallback, useState} from "react";
import {ValueObjectOutput} from "src/utils/types";
import CloseIcon from '@mui/icons-material/Close';
import {api} from "src/utils/api";

type Props = {
    open: boolean,
    onClose: () => void,
    onSuccess: () => void,
    toStorage: ValueObjectOutput | null,
    storages: Array<ValueObjectOutput>,
    products: Array<ValueObjectOutput>,
}

function CreateMovementDialog({ open, onClose, onSuccess, toStorage, storages, products }: Props) {
    const [error, setError] = useState<string | null>(null);

    const [fromStorageId, setFromStorageId] = useState<string | null>(null);
    const [toStorageId, setToStorageId] = useState<string | null>(toStorage?.id ?? null);
    const [movedProducts, setMovedProducts] = useState<Map<string, number>>(new Map());

    const canAddProduct = useCallback(() => movedProducts.size !== products.length, [products, movedProducts]);

    const changeProductCount = useCallback((productId: string, count: number) => setMovedProducts(new Map(movedProducts.set(productId, count))), [movedProducts]);
    const removeProduct = useCallback((productId) => {
        movedProducts.delete(productId);
        setMovedProducts(new Map(movedProducts));
    }, [movedProducts]);

    const productName = useCallback((id: string) => products.find(it => it.id === id)?.name, [products]);

    const butStorage = useCallback((storageId: string | null) => storages.filter(it => it.id !== storageId), [storages]);

    const movedProductsAsArray = useCallback(() => Array.from(movedProducts.entries()), [movedProducts])

    const handleClose = useCallback(() => {
        setFromStorageId(null);
        setToStorageId(null);
        setMovedProducts(new Map());
        onClose();
    }, [onClose]);

    const createMovement = useCallback(() => {
        if (!toStorageId) {
            setError(`Необходимо выбрать на какой склад перемещаем товары`);
            return;
        }

        if (!movedProducts.size) {
            setError(`Необходимо указать товары`);
            return;
        }

        const productInputs = Array.from(movedProducts.entries()).map(([id, count]) => ({ id, count }))

        api.createMovement({ fromStorageId, toStorageId, products: productInputs })
            .then(onSuccess)
            .catch(e => {
                if (e?.response?.data?.message) {
                    setError(String(e?.response?.data?.message));
                }
            });

    }, [fromStorageId, toStorageId, movedProducts, setError, onSuccess]);

    return <Dialog onClose={handleClose} open={open}>
        <DialogTitle>
            {toStorageId
                ? fromStorageId
                    ? <>Перемещение <b>{storages.find(it => it.id === fromStorageId)?.name}</b> {'->'} <b>{storages.find(it => it.id === toStorageId)?.name}</b></>
                    : <>Добавление на склад <b>{storages.find(it => it.id === toStorageId)?.name}</b></>
                : <>Перемещение товаров</>
            }
        </DialogTitle>

        <DialogContent style={{ paddingTop: '20px' }}>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <FormControl fullWidth>
                        <InputLabel id="from-storage-id">Со склада</InputLabel>
                        <Select
                            labelId="from-storage-id"
                            value={fromStorageId}
                            label="Склад"
                            onChange={it => setFromStorageId(it.target.value)}
                        >
                            <MenuItem value=""><em>Не выбрано</em></MenuItem>
                            {butStorage(toStorageId).map(it => <MenuItem value={it.id}>{it.name}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Grid>

                <Grid item xs={6}>
                    <FormControl fullWidth>
                        <InputLabel id="to-storage-id">На склад</InputLabel>
                        <Select
                            labelId="to-storage-id"
                            value={toStorageId}
                            label="Склад"
                            onChange={it => setToStorageId(it.target.value)}
                        >
                            {butStorage(fromStorageId).map(it => <MenuItem value={it.id}>{it.name}</MenuItem>)}
                        </Select>
                    </FormControl>
                </Grid>

                {movedProductsAsArray().map(([productId, count]) => {
                    return <>
                        <Grid item xs={7}>
                            <FormControl fullWidth>
                                <FormLabel>
                                    {productName(productId)}
                                </FormLabel>
                            </FormControl>
                        </Grid>

                        <Grid item xs={3}>
                            <FormControl fullWidth>
                                <Input type="numeric" value={count} onChange={e => changeProductCount(productId, Number(e.target.value ?? 1))} />
                            </FormControl>
                        </Grid>

                        <Grid item xs={1}>
                            <Button onClick={() => removeProduct(productId)}>
                                <CloseIcon />
                            </Button>
                        </Grid>
                    </>
                })}

                {canAddProduct() && <Grid item xs={12}>
                    <FormControl fullWidth>
                        <InputLabel id="add-products">Добавить товар</InputLabel>
                        <Select labelId="add-products" variant="outlined">
                            {products
                                .filter(it => !movedProducts.has(it.id))
                                .map(it =>
                                    <MenuItem key={it.id} onClick={() => changeProductCount(it.id, 1)}>
                                        {it.name}
                                    </MenuItem>
                                )}
                        </Select>
                    </FormControl>
                </Grid>}
            </Grid>
        </DialogContent>
        <DialogActions>
            <Button onClick={createMovement}>Применить</Button>
        </DialogActions>

        <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            open={!!error}
            autoHideDuration={6000}
            onClose={() => setError(null)}
        >
            <Alert severity="error">{error}</Alert>
        </Snackbar>
    </Dialog>;
}

export default CreateMovementDialog;

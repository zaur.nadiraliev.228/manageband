import {DateTimePicker} from "@mui/lab";
import {Box, Button, CircularProgress, Dialog, DialogContent, FormControl, Grid, TextField} from "@mui/material";
import {useCallback, useEffect, useState} from "react";
import {api} from "src/utils/api";
import {MovedProductOutput} from "src/utils/types";

type Props = {
    open: boolean,
    onClose: () => void,
    storageId: string,
}

function StorageBalanceDialog({ open, onClose, storageId }: Props) {
    const [onDate, setOnDate] = useState(new Date());
    const [isLoading, setLoading] = useState(false);
    const [products, setProducts] = useState<MovedProductOutput[]>([]);

    const loadProducts = useCallback(() => {
        setLoading(true);

        api.getStorageBalance(storageId, onDate)
            .then(setProducts)
            .finally(() => setLoading(false));

    }, [storageId, onDate, setProducts]);

    useEffect(() => {
        if (open) {
            loadProducts();
            setOnDate(new Date());
        }
    }, [open, setOnDate])

    const handleClose = useCallback(() => {
        setOnDate(new Date());
        setProducts([]);
        setLoading(false);
        onClose();
    }, [onClose, setOnDate, setProducts, setLoading])

    return <Dialog open={open} onClose={handleClose}>
        <DialogContent>
            <Grid container spacing={3}>
                <Grid item xs={8}>
                    <FormControl>
                        <DateTimePicker
                            renderInput={(props) => <TextField {...props} />}
                            label="DateTimePicker"
                            value={onDate}
                            onChange={it => { setOnDate(it ?? new Date()); }}
                        />
                    </FormControl>
                </Grid>

                <Grid item xs={4}>
                    <Button onClick={loadProducts}>Загрузить данные</Button>
                </Grid>

                {isLoading
                    ? <Grid item xs={12}>
                        <Box sx={{ display: 'flex' }}>
                            <CircularProgress />
                        </Box>
                    </Grid>
                    : <>
                        {products.length
                            ? <>
                                {products.map(it =>
                                    <Grid key={it.id} item xs={12}>
                                        {it.name} - {it.count}
                                    </Grid>
                                )}
                            </>
                            : <Grid item xs={12}>Нет данных</Grid>
                        }
                    </>}
            </Grid>
        </DialogContent>
    </Dialog>;
};

export default StorageBalanceDialog;

import axios from "axios";
import { MovedProductOutput, MovementInput, MovementOutput, PaginatedOutput, Pagination, ValueObjectOutput } from "./types";

const url = 'http://localhost:5000'; // fixme harcoded

export const api = {
    getMovements: (pagination: Pagination) => {
        return axios
            .get<PaginatedOutput<MovementOutput>>(`${url}/movements`, { params: pagination })
            .then(it => it.data);
    },

    createMovement: (input: MovementInput) => {
        return axios.post<MovementOutput>(`${url}/movements`, input);
    },

    getProducts: (pagination: Pagination) => {
        return axios.get<PaginatedOutput<ValueObjectOutput>>(`${url}/products`, { params: pagination }).then(it => it.data);
    },

    getStorages: (pagination: Pagination) => {
        return axios.get<PaginatedOutput<ValueObjectOutput>>(`${url}/storages`, { params: pagination }).then(it => it.data);
    },

    getStorageBalance: (storageId: string, onDate: Date) => {
        return axios.get<MovedProductOutput[]>(`${url}/storages/${storageId}/balance`, { params: { onDate } }).then(it => it.data);
    }
}

export type Pagination = {
    limit: number,
    offset: number
};

export type PaginatedOutput<T> = {
    totalCount: number,
    values: Array<T>
}

export type MovementInput = {
    fromStorageId?: string | null,
    toStorageId: string,
    products: Array<{ id: string, count: number }>
}

export type ValueObjectOutput = {
    id: string,
    name: string,
}

export type MovedProductOutput = {
    id: string,
    name: string,
    count: number
}

export type MovementOutput = {
    id: string,
    from: ValueObjectOutput | null,
    to: ValueObjectOutput,
    products: Array<MovedProductOutput>,
    createdAt: string,
}


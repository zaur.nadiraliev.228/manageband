import { Button, Grid } from "@mui/material";
import { useCallback, useEffect, useState } from "react";
import CreateMovementDialog from "src/CreateMovementDialog/CreateMovementDialog";
import { api } from "src/utils/api";
import { MovementOutput, ValueObjectOutput } from "src/utils/types";
import AddIcon from '@mui/icons-material/Add';
import AnalyticsIcon from '@mui/icons-material/Analytics';
import StorageBalanceDialog from "src/StorageBalanceDialog/StorageBalanceDialog";

type Props = {
    storages: ValueObjectOutput[],
    products: ValueObjectOutput[],
}

function MainPage({ storages, products }: Props) {
    const [dialogOpen, setDialogOpen] = useState(false);
    const [balanceDialogOpen, setBalanceDialogOpen] = useState(false);
    const [movements, setMovements] = useState<MovementOutput[]>([]);

    const [selectedStorage, setSelectedStorage] = useState<ValueObjectOutput | null>(null);

    const loadMovements = useCallback(() => {
        api.getMovements({ limit: 100, offset: 0 }).then(it => setMovements(it.values));
    }, [setMovements]);

    useEffect(() => loadMovements(), [loadMovements]);

    return <div>
        <CreateMovementDialog
            open={dialogOpen}
            onSuccess={() => { setDialogOpen(false); loadMovements(); }}
            onClose={() => setDialogOpen(false)}
            products={products}
            storages={storages}
            toStorage={selectedStorage}
        />

        {selectedStorage && <StorageBalanceDialog
            open={balanceDialogOpen}
            onClose={() => setBalanceDialogOpen(false)}
            storageId={selectedStorage.id}
        />}

        <Grid container spacing={3}>
            {storages.map(it => {
                return <Grid item xs={3}>
                    <>{it.name}</>
                    <Button onClick={() => { setSelectedStorage(it); setDialogOpen(true); }}><AddIcon/></Button>
                    <Button onClick={() => { setSelectedStorage(it); setBalanceDialogOpen(true); }}><AnalyticsIcon /></Button>
                </Grid>
            })}
        </Grid>

        <Grid container spacing={2} style={{ marginTop: '30px' }}>
            <Grid item xs={8}>
                {movements.length === 0
                    ? <h3>Нет перемещений товаров</h3>
                    : <Grid container spacing={3} gap={10}>
                        {movements.map(it =>    
                            <Grid key={it.id} item xs={3} style={{ border: '1px solid silver' }}>
                                <p>{it.createdAt}</p>
                                <p>
                                    {it.from && it.from.name + ` -> `}
                                    {it.to.name}
                                </p>
                                {it.products.map(p => 
                                    <p key={p.id}>
                                        {p.name} - {p.count}
                                    </p>
                                )}
                            </Grid>
                        )}
                    </Grid>}
            </Grid>
            <Grid item xs={4}>
                <Button onClick={() => { setSelectedStorage(null); setDialogOpen(true); }} variant="contained"> Добавить на склад </Button>
            </Grid>
        </Grid>
    </div>;
}

export default MainPage;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Warehouse.Infrastructure.EFCore;

namespace Warehouse.Infrastructure
{
    public static class InfrastructureExtension
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
            EfDbSettings efDbSettings)
        {
            services
                .AddDbContext<MainDbContext>(o => o.UseNpgsql(efDbSettings.ToConnectionString()));

            return services;
        }
    }
}
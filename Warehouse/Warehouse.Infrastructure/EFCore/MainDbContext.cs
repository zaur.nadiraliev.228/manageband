using Microsoft.EntityFrameworkCore;
using Warehouse.Domain;

#nullable disable

namespace Warehouse.Infrastructure.EFCore
{
    public class MainDbContext : DbContext
    {
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Storage> Storages { get; set; }
        public virtual DbSet<Movement> Movements { get; set; }
        public virtual DbSet<MovedProduct> MovedProducts { get; set; }

        public MainDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>(e =>
            {
                e.HasKey(it => it.Id);
                e.Property(it => it.Name).IsRequired();
            });

            modelBuilder.Entity<Storage>(e =>
            {
                e.HasKey(it => it.Id);
                e.Property(it => it.Name).IsRequired();
                e.HasMany(it => it.FromStorages).WithOne(it => it.From);
                e.HasMany(it => it.ToStorages).WithOne(it => it.To);
            });

            modelBuilder.Entity<Movement>(e =>
            {
                e.HasKey(it => it.Id);
                e.HasOne(it => it.From).WithMany(it => it.FromStorages);
                e.HasOne(it => it.To).WithMany(it => it.ToStorages).IsRequired();
            });

            modelBuilder.Entity<MovedProduct>(e =>
            {
                e.HasKey(it => it.Id);
                e.Property(it => it.Count).IsRequired();
                e.HasOne(it => it.Movement).WithMany(it => it.Products).IsRequired();
                e.HasOne(it => it.Product);
            });
        }
    }
}
namespace Warehouse.Infrastructure.EFCore
{
    public record EfDbSettings(string Host = "", string Database = "", string Username = "", string Password = "")
    {
        public string ToConnectionString()
        {
            return $"Host={Host};Database={Database};Username={Username};Password={Password}";
        }
    }
}
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Warehouse.Infrastructure.Pagination
{
    public static class PaginationExtension
    {
        public static async Task<PaginatedOutput<T>> PaginateAsync<T>(this IQueryable<T> values, Pagination pagination)
        {
            var totalCount = values.Count();

            var loadedValues = await values
                .Take(pagination.Limit)
                .Skip(pagination.Offset)
                .ToListAsync();

            return new PaginatedOutput<T>(totalCount, loadedValues);
        }
    }
}
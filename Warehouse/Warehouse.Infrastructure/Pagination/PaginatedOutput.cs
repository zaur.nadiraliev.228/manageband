using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Warehouse.Infrastructure.Pagination
{
    public record PaginatedOutput<T>(int TotalCount, IList<T> Values)
    {
        public PaginatedOutput<TMapped> Map<TMapped>(Func<T, TMapped> mapper)
        {
            return new PaginatedOutput<TMapped>(TotalCount, Values.Select(mapper).ToList());
        }

        public async Task<PaginatedOutput<TMapped>> MapAsync<TMapped>(Func<T, Task<TMapped>> mapper)
        {
            var values = new List<TMapped>();

            foreach (var value in Values)
            {
                values.Add(await mapper(value));
            }

            return new PaginatedOutput<TMapped>(TotalCount, values);
        }
    }
}
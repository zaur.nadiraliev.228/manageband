namespace Warehouse.Infrastructure.Pagination
{
    public record Pagination(int Limit = 100, int Offset = 0);
}
using System;

namespace Warehouse.Domain
{
    public record Product(
        Guid Id,
        string Name
    );
}
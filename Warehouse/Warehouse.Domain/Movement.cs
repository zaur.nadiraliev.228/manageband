using System;
using System.Collections.Generic;
using System.Linq;

namespace Warehouse.Domain
{
    public record Movement(
        Guid Id,
        Storage? From,
        Storage To,
        IList<MovedProduct> Products,
        DateTime CreatedAt
    )
    {
        public void AddProduct(Product product, uint count)
        {
            var isProductAlreadyAdded = Products.Select(it => it.Product.Id).Contains(product.Id);

            if (isProductAlreadyAdded)
            {
                return;
            }

            var movedProduct = new MovedProduct(Guid.NewGuid(), product, count, this);

            Products.Add(movedProduct);
        }
    }

    public record MovedProduct(
        Guid Id,
        Product Product,
        uint Count,
        Movement Movement
    );
}
using System;
using System.Collections.Generic;

namespace Warehouse.Domain
{
    public record Storage(
        Guid Id,
        string Name
    )
    {
        public virtual IList<Movement> FromStorages { get; set; }
        public virtual IList<Movement> ToStorages { get; set; }
    }
}
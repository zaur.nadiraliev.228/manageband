using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Warehouse.Application.IO;
using Warehouse.Application.Query;
using Warehouse.Domain;
using Warehouse.Infrastructure.EFCore;
using static Warehouse.Application.Exception.ApplicationErrors;
using ApplicationException = Warehouse.Application.Exception.ApplicationException;

namespace Warehouse.Application.Command
{
    public class CreateMovementCommand
    {
        private readonly CalculateStorageBalanceByProductQuery _calculateStorageBalance;
        private readonly MainDbContext _dbContext;

        public CreateMovementCommand(
            CalculateStorageBalanceByProductQuery calculateStorageBalance,
            MainDbContext dbContext
        )
        {
            _calculateStorageBalance = calculateStorageBalance;
            _dbContext = dbContext;
        }

        public async Task Execute(MovementInput input)
        {
            var fromStorage = await _dbContext.Storages.FindAsync(input.FromStorageId ?? Guid.Empty);
            var toStorage = await _dbContext.Storages.FindAsync(input.ToStorageId);

            if (null == toStorage)
            {
                throw new ApplicationException(NotFound, $"Storage with id '{input.ToStorageId}' was not found");
            }

            if (null != fromStorage)
            {
                await AssertStorageHasProductsAsync(fromStorage, input.Products);
            }

            var productToCount = input.Products.ToDictionary(it => it.Id, it => it.Count);
            var productIds = input.Products.Select(it => it.Id);

            var foundProducts = await _dbContext.Products.Where(it => productIds.Contains(it.Id)).ToListAsync();

            if (!foundProducts.Any())
            {
                throw new ApplicationException(NotFound, "There are no products to move found");
            }

            var newMovement = CreateMovement(fromStorage, toStorage, foundProducts, productToCount);

            await _dbContext.AddAsync(newMovement);
            await _dbContext.SaveChangesAsync();
        }

        private Movement CreateMovement(
            Storage? fromStorage,
            Storage toStorage,
            IEnumerable<Product> foundProducts,
            IDictionary<Guid, uint> productToCount
        )
        {
            var movement = new Movement(
                Guid.NewGuid(),
                fromStorage,
                toStorage,
                new List<MovedProduct>(),
                DateTime.UtcNow
            );

            foreach (var product in foundProducts)
            {
                movement.AddProduct(product, productToCount[product.Id]);
            }

            return movement;
        }

        private async Task AssertStorageHasProductsAsync(Storage storage, IList<MovedProductInput> products)
        {
            foreach (var (productId, count) in products)
            {
                var remainedOnStorageCount = await _calculateStorageBalance.Execute(storage.Id, productId);

                if (remainedOnStorageCount < count)
                {
                    throw new ApplicationException(NotEnoughProductCount,
                        $"Not enough product count, required {count}, remained {remainedOnStorageCount}");
                }
            }
        }
    }
}
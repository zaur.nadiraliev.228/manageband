using Microsoft.Extensions.DependencyInjection;
using Warehouse.Application.Command;
using Warehouse.Application.Query;
using Warehouse.Application.Utility;

namespace Warehouse.Application
{
    public static class ApplicationExtension
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services
                .AddTransient<GetMovementsQuery>()
                .AddTransient<CalculateStorageBalanceByProductQuery>()
                .AddTransient<CalculateStorageBalanceOnDateQuery>()
                .AddTransient<GetProductsQuery>()
                .AddTransient<GetStoragesQuery>()
                .AddTransient<CreateMovementCommand>();

            services.AddTransient<FixturesService>();

            return services;
        }
    }
}
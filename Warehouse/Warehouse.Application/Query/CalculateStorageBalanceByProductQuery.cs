using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Warehouse.Domain;
using Warehouse.Infrastructure.EFCore;

namespace Warehouse.Application.Query
{
    public class CalculateStorageBalanceByProductQuery
    {
        private readonly MainDbContext _dbContext;

        public CalculateStorageBalanceByProductQuery(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<long> Execute(Guid storageId, Guid productId)
        {
            var productsRemoved = await FindProductsAsync(productId, it => it.From != null && it.From.Id == storageId);
            var productsArrived = await FindProductsAsync(productId, it => it.To.Id == storageId);

            long Sum(IEnumerable<MovedProduct> products) => products.Sum(it => it.Count);

            var remainedCount = Sum(productsArrived) + Sum(productsRemoved);

            return remainedCount > 0 ? remainedCount : 0;
        }

        private Task<List<MovedProduct>> FindProductsAsync(Guid productId,
            Expression<Func<Movement, bool>> filterStorage)
        {
            return _dbContext.Movements
                .Where(filterStorage)
                .SelectMany(it => it.Products)
                .Where(it => it.Product.Id == productId)
                .ToListAsync();
        }
    }
}
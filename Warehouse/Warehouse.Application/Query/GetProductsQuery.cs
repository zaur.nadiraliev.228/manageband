using System.Linq;
using System.Threading.Tasks;
using Warehouse.Application.IO;
using Warehouse.Infrastructure.EFCore;
using Warehouse.Infrastructure.Pagination;

namespace Warehouse.Application.Query
{
    public class GetProductsQuery
    {
        private readonly MainDbContext _dbContext;

        public GetProductsQuery(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<PaginatedOutput<ValueObjectOutput>> Execute(Pagination pagination)
        {
            var storages = await _dbContext.Products
                .OrderBy(it => it.Name)
                .PaginateAsync(pagination);

            return storages.Map(it => new ValueObjectOutput(it));
        }
    }
}
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Warehouse.Application.IO;
using Warehouse.Infrastructure.EFCore;
using Warehouse.Infrastructure.Pagination;

namespace Warehouse.Application.Query
{
    public class GetMovementsQuery
    {
        private readonly MainDbContext _dbContext;

        public GetMovementsQuery(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PaginatedOutput<MovementOutput>> Execute(Pagination pagination)
        {
            var movements = await _dbContext.Movements
                .OrderByDescending(it => it.CreatedAt)
                .Include(it => it.From)
                .Include(it => it.To)
                .Include(it => it.Products).ThenInclude(it => it.Product)
                .PaginateAsync(pagination);

            return movements.Map(it => new MovementOutput(it));
        }
    }
}   
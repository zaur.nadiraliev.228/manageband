using System;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Application.IO;
using Warehouse.Infrastructure.EFCore;
using Warehouse.Infrastructure.Pagination;

namespace Warehouse.Application.Query
{
    public class GetStoragesQuery
    {
        private readonly MainDbContext _dbContext;

        public GetStoragesQuery(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<PaginatedOutput<ValueObjectOutput>> Execute(Pagination pagination)
        {
            var storages = await _dbContext.Storages
                .OrderBy(it => it.Name)
                .PaginateAsync(pagination);

            return storages.Map(it => new ValueObjectOutput(it));
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Warehouse.Application.IO;
using Warehouse.Domain;
using Warehouse.Infrastructure.EFCore;

namespace Warehouse.Application.Query
{
    public class CalculateStorageBalanceOnDateQuery
    {
        private readonly MainDbContext _dbContext;

        public CalculateStorageBalanceOnDateQuery(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public virtual async Task<IList<MovedProductOutput>> Execute(Guid storageId, DateTime onDate)
        {
            var allArrivedProducts = await FindProductsAsync(onDate, it => it.To.Id == storageId);
            var allRemovedProducts = await FindProductsAsync(onDate, it => it.From != null && it.From.Id == storageId);

            var arrivedProductsEachCount = SumCountGroupedByProduct(allArrivedProducts);
            var removedProductsEachCount = SumCountGroupedByProduct(allRemovedProducts);

            return arrivedProductsEachCount.Select(it =>
                {
                    var (product, count) = it.Value;

                    var toMinus = removedProductsEachCount.ContainsKey(product.Id)
                        ? removedProductsEachCount[product.Id].count
                        : 0;

                    return new MovedProductOutput(product.Id, product.Name, count - toMinus);
                })
                .ToList();
        }

        private Task<List<MovedProduct>> FindProductsAsync(DateTime onDate, Expression<Func<Movement, bool>> filterStorage)
        {
            return _dbContext.Movements
                .Where(filterStorage)
                .Where(it => it.CreatedAt <= onDate)
                .SelectMany(it => it.Products)
                .Include(it => it.Product)
                .ToListAsync();
        }

        private static Dictionary<Guid, (Product product, long count)> SumCountGroupedByProduct(
            IEnumerable<MovedProduct> productsArrived)
        {
            return productsArrived.GroupBy(it => it.Product)
                .ToDictionary(it => it.Key.Id, it => (it.Key, it.Sum(v => v.Count)));
        }
    }
}
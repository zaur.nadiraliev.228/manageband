using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Warehouse.Domain;
using Warehouse.Infrastructure.EFCore;

namespace Warehouse.Application.Utility
{
    public class FixturesService
    {
        private readonly string[] _storages = {
            "Московский", "Питерский", "Минский",
        };
        
        private readonly string[] _products = {
            "Стул", "Стол", "Книга", "Кровать", "Шкаф", "Дверь", "Диван",
        };

        private readonly MainDbContext _dbContext;

        public FixturesService(MainDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task FillPresetDataAsync()
        {
            var storages = _storages.Select(it => new Storage(Guid.NewGuid(), it));
            var products = _products.Select(it => new Product(Guid.NewGuid(), it));

            await _dbContext.AddRangeAsync(storages);
            await _dbContext.AddRangeAsync(products);

            await _dbContext.SaveChangesAsync();
        }
    }
}
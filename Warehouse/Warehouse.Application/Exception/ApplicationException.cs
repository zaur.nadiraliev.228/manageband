namespace Warehouse.Application.Exception
{
    public class ApplicationException : System.Exception
    {
        public ApplicationErrors ErrorType { get; }

        public ApplicationException(ApplicationErrors errorType, string message) : base(message)
        {
            ErrorType = errorType;
        }
    }

    public enum ApplicationErrors
    {
        NotFound,
        NotEnoughProductCount,
        Undefined
    }
}
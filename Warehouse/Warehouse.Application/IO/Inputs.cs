using System;
using System.Collections.Generic;

namespace Warehouse.Application.IO
{
    public record MovementInput(
        Guid? FromStorageId,
        Guid ToStorageId,
        IList<MovedProductInput> Products
    );

    public record MovedProductInput(Guid Id, uint Count);
}
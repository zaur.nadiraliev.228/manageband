using System;
using System.Collections.Generic;
using System.Linq;
using Warehouse.Domain;

namespace Warehouse.Application.IO
{
    public record ValueObjectOutput(Guid Id, string Name)
    {
        public ValueObjectOutput(Product product) : this(product.Id, product.Name)
        {
        }

        public ValueObjectOutput(Storage storage) : this(storage.Id, storage.Name)
        {
        }
    }

    public class MovementOutput
    {
        public Guid Id { get; }
        public ValueObjectOutput? From { get; }
        public ValueObjectOutput To { get; }
        public IList<MovedProductOutput> Products { get; }
        public DateTime CreatedAt { get; }

        public MovementOutput(Movement movement)
        {
            Id = movement.Id;
            To = new ValueObjectOutput(movement.To);
            CreatedAt = movement.CreatedAt;

            From = null != movement.From
                ? new ValueObjectOutput(movement.From)
                : null;

            Products = movement.Products
                .Select(it => new MovedProductOutput(it.Product.Id, it.Product.Name, it.Count))
                .ToList();
        }
    }

    public record MovedProductOutput(Guid Id, string Name, long Count);
}
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Warehouse.Application;
using Warehouse.Application.Utility;
using Warehouse.Infrastructure;
using Warehouse.Infrastructure.EFCore;

namespace Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var efSettings = new EfDbSettings();

            _configuration.Bind("EFCore", efSettings);

            services.AddControllers();
            services.AddApplicationServices();
            services.AddInfrastructureServices(efSettings);
            services.AddTransient<ExceptionMiddleware>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, MainDbContext dbContext)
        {
            dbContext.Database.EnsureCreated();

            FillEntityPresetsIfEmpty(dbContext, new FixturesService(dbContext));

            app.UseCors(o => o.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            app.UseMiddleware<ExceptionMiddleware>();
            app.UseRouting();
    
            app.UseEndpoints(endpoints => endpoints.MapControllers());
        }

        private static void FillEntityPresetsIfEmpty(MainDbContext dbContext, FixturesService service)
        {
            var productsCount = dbContext.Products.Count();
            var storagesCount = dbContext.Storages.Count();

            if (productsCount == 0 && storagesCount == 0)
            {
                service.FillPresetDataAsync().Wait();
            }
        }
    }
}
using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using ApplicationException = Warehouse.Application.Exception.ApplicationException;

namespace Api
{
    public class ExceptionMiddleware : IMiddleware
    {
        private static JsonSerializerOptions _options;

        static ExceptionMiddleware()
        {
            _options = new()
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase
            };
        }
        
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                context.Response.Headers.Add("Content-Type", "application/json");

                context.Response.StatusCode = exception is ApplicationException
                    ? StatusCodes.Status400BadRequest
                    : StatusCodes.Status500InternalServerError;

                await context.Response.WriteAsync(CreateErrorResponseString(exception));
            }
        }

        private static string CreateErrorResponseString(Exception ex) =>
            JsonSerializer.Serialize(new { ErrorType = ex.GetType().Name, ex.Message }, _options);
    }
}
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Application.IO;
using Warehouse.Application.Query;
using Warehouse.Infrastructure.Pagination;

namespace Api.Controllers
{
    [ApiController]
    [Route("products")]
    public class ProductController
    {
        [HttpGet]
        public Task<PaginatedOutput<ValueObjectOutput>> GetProductsAsync(
            [FromServices] GetProductsQuery query,
            [FromQuery] Pagination? pagination = null
        )
        {
            return query.Execute(pagination ?? new Pagination());
        }
    }
}
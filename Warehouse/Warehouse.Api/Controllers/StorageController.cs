using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Application.IO;
using Warehouse.Application.Query;
using Warehouse.Infrastructure.Pagination;

namespace Api.Controllers
{
    [ApiController]
    [Route("storages")]
    public class StorageController
    {
        [HttpGet]
        public Task<PaginatedOutput<ValueObjectOutput>> GetStoragesAsync(
            [FromServices] GetStoragesQuery query,
            [FromQuery] Pagination? pagination = null
        )
        {
            return query.Execute(pagination ?? new Pagination());
        }

        [HttpGet("{storageId:guid}/balance")]
        public Task<IList<MovedProductOutput>> GetStorageBalanceAsync(
            [FromServices] CalculateStorageBalanceOnDateQuery query,
            [FromRoute] Guid storageId,
            [FromQuery] DateTime? onDate = null
        )
        {
            return query.Execute(storageId, onDate ?? DateTime.UtcNow);
        }
    }
}
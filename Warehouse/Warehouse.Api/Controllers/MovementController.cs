using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Warehouse.Application.Command;
using Warehouse.Application.IO;
using Warehouse.Application.Query;
using Warehouse.Infrastructure.Pagination;

namespace Api.Controllers
{
    [ApiController]
    [Route("movements")]
    public class MovementController
    {
        [HttpGet]
        public Task<PaginatedOutput<MovementOutput>> GetMovementsAsync([FromServices] GetMovementsQuery query, [FromQuery] Pagination pagination)
        {
            return query.Execute(pagination);
        }

        [HttpPost]
        public Task CreateMovementAsync([FromServices] CreateMovementCommand command, [FromBody] MovementInput input)
        {
            return command.Execute(input);
        }
    }
}
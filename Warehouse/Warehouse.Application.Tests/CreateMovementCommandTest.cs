using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MockQueryable.Moq;
using Moq;
using NUnit.Framework;
using Warehouse.Application.Command;
using Warehouse.Application.Exception;
using Warehouse.Application.IO;
using Warehouse.Application.Query;
using Warehouse.Domain;
using Warehouse.Infrastructure.EFCore;
using ApplicationException = Warehouse.Application.Exception.ApplicationException;

namespace Warehouse.Application.Tests
{
    public class CreateMovementCommandTest
    {
        private Mock<CalculateStorageBalanceByProductQuery> _calculateStorageBalanceQuery;
        private Mock<MainDbContext> _mainDbContext;
        private CreateMovementCommand _command;

        [SetUp]
        public void Setup()
        {
            _calculateStorageBalanceQuery = new Mock<CalculateStorageBalanceByProductQuery>();
            _mainDbContext = new Mock<MainDbContext>();

            _command = new CreateMovementCommand(
                _calculateStorageBalanceQuery.Object,
                _mainDbContext.Object
            );
        }

        [Test]
        public void Test_ExceptionException_WhenToStorageIsNull()
        {
            _mainDbContext.Setup(it => it.Storages).Returns(new List<Storage>().AsQueryable().BuildMockDbSet().Object);

            Assert.That(
                async () => await _command.Execute(new MovementInput(null, Guid.Empty, new List<MovedProductInput>())),
                Throws.TypeOf<ApplicationException>()
                    .And.Property(nameof(ApplicationException.ErrorType))
                    .EqualTo(ApplicationErrors.NotFound)
            );
        }

        [Test]
        public void Test_ExceptException_WhenFromStorageHasNotEnoughProducts()
        {
            var fromStorage = new Storage(Guid.NewGuid(), "From");
            var toStorage = new Storage(Guid.NewGuid(), "To");

            var storages = new[] {fromStorage, toStorage}.AsQueryable().BuildMockDbSet();

            storages.Setup(it => it.FindAsync(fromStorage.Id)).Returns(ValueTask.FromResult(fromStorage));
            storages.Setup(it => it.FindAsync(toStorage.Id)).Returns(ValueTask.FromResult(toStorage));

            _mainDbContext.Setup(it => it.Storages).Returns(storages.Object);

            _calculateStorageBalanceQuery
                .Setup(it => it.Execute(It.IsAny<Guid>(), It.IsAny<Guid>()))
                .Returns(Task.FromResult(0L));

            Assert.That(
                async () => await _command.Execute(new MovementInput(
                    FromStorageId: fromStorage.Id,
                    ToStorageId: toStorage.Id,
                    new List<MovedProductInput>(new[] {new MovedProductInput(Guid.Empty, 1)})
                )),
                Throws.TypeOf<ApplicationException>()
                    .And.Property(nameof(ApplicationException.ErrorType))
                    .EqualTo(ApplicationErrors.NotEnoughProductCount)
            );
        }

        [Test]
        public void Test_ExceptException_WhenNoProductsFoundBySpecifiedIds()
        {
            var toStorage = new Storage(Guid.NewGuid(), "To");

            var storages = new[] {toStorage};
            var products = new List<Product>();

            _mainDbContext.Setup(it => it.Storages).Returns(storages.AsQueryable().BuildMockDbSet().Object);
            _mainDbContext.Setup(it => it.Products).Returns(products.AsQueryable().BuildMockDbSet().Object);

            Assert.That(
                async () => await _command.Execute(new MovementInput(
                    FromStorageId: null,
                    ToStorageId: toStorage.Id,
                    new List<MovedProductInput>()
                )),
                Throws.TypeOf<ApplicationException>()
                    .And.Property(nameof(ApplicationException.ErrorType))
                    .EqualTo(ApplicationErrors.NotFound)
            );
        }

        [Test]
        public async Task Test_CreateMovement_WhenFromStorageIsNotSpecified()
        {
            var toStorage = new Storage(Guid.NewGuid(), "To");
            var product = new Product(Guid.NewGuid(), "Product");

            var storages = new[] {toStorage};
            var products = new[] {product};

            var storageMock = storages.AsQueryable().BuildMockDbSet();
            storageMock.Setup(it => it.FindAsync(toStorage.Id)).Returns(ValueTask.FromResult(toStorage));

            _mainDbContext.Setup(it => it.Storages).Returns(storageMock.Object);
            _mainDbContext.Setup(it => it.Products).Returns(products.AsQueryable().BuildMockDbSet().Object);

            await _command.Execute(new MovementInput(
                FromStorageId: null,
                ToStorageId: toStorage.Id,
                Products: new List<MovedProductInput>(new[] {new MovedProductInput(product.Id, 5)})
            ));

            _calculateStorageBalanceQuery.Verify(it => it.Execute(It.IsAny<Guid>(), It.IsAny<Guid>()),
                Times.Never);

            _mainDbContext.Verify(it => it.AddAsync(It.IsAny<Movement>(), It.IsAny<CancellationToken>()), Times.Once);
            _mainDbContext.Verify(it => it.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [Test]
        public async Task Test_CreateMovement_WhenBothStoragesSpecified()
        {
            var toStorage = new Storage(Guid.NewGuid(), "To");
            var fromStorage = new Storage(Guid.NewGuid(), "From");
            var product = new Product(Guid.NewGuid(), "Product");

            var storages = new[] {fromStorage, toStorage};
            var products = new[] {product};

            var storageMock = storages.AsQueryable().BuildMockDbSet();
            storageMock.Setup(it => it.FindAsync(toStorage.Id)).Returns(ValueTask.FromResult(toStorage));
            storageMock.Setup(it => it.FindAsync(fromStorage.Id)).Returns(ValueTask.FromResult(fromStorage));
            
            _mainDbContext.Setup(it => it.Storages).Returns(storageMock.Object);
            _mainDbContext.Setup(it => it.Products).Returns(products.AsQueryable().BuildMockDbSet().Object);

            _calculateStorageBalanceQuery
                .Setup(it => it.Execute(fromStorage.Id, product.Id))
                .Returns(Task.FromResult(100L));

            await _command.Execute(new MovementInput(
                FromStorageId: fromStorage.Id,
                ToStorageId: toStorage.Id,
                Products: new List<MovedProductInput>(new[] {new MovedProductInput(product.Id, 5)})
            ));

            _mainDbContext.Verify(it => it.AddAsync(It.IsAny<Movement>(), It.IsAny<CancellationToken>()), Times.Once);
            _mainDbContext.Verify(it => it.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}